﻿namespace DialOgBoxTestApp;

interface

uses
  System.Collections.Generic,
  System.Windows,
  System.Windows.Controls,
  System.Windows.Data,
  System.Windows.Documents,
  System.Windows.Media,
  System.Windows.Navigation,
  System.Windows.Shapes,
  MahApps.Metro;

type
  Window1 = public partial class(System.Windows.Window)
  private
    method Button_Click(sender: Object; e: System.Windows.RoutedEventArgs);
  public
    constructor;
  end;
  
implementation

constructor Window1;
begin
  InitializeComponent();
end;

method Window1.Button_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  MessageBox.Show(self,"Re-reading the MicroTiter Plates will cause new images of the Wells to be taken and evaluated. The evaluation of the images will be stored as new results for the Samples. No prior results for the Samples will be overwritten." +
                                                              Environment.NewLine +"Would you like to continue?","Re-Read Microtiter Plates",MessageBoxButton.YesNo,MessageBoxImage.Question);
  MessageBox.Show(self, 'Dit is een test een heel lange test met name, eens zien wat het resultaat is.', 'Dit is de caption', MessageBoxButton.YesNoCancel, MessageBoxImage.Information);
  MessageBox.Show(self, 'Dit is een test een heel lange test met name, eens zien wat het resultaat is.', 'Dit is de caption', MessageBoxButton.YesNoCancel, MessageBoxImage.None);
  MessageBox.Show(self, 'Dit is een test een heel lange test met name, eens zien wat het resultaat is.', 'Dit is de caption', MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
  MessageBox.Show(self, 'Dit is een test een heel lange test met name, eens zien wat het resultaat is.', 'Dit is de caption', MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
  MessageBox.Show(self, 'Dit is een test een heel lange test met name, eens zien wat het resultaat is.', 'Dit is de caption', MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
  MessageBox.Show(self, 'Korte tekst', 'Dit is de caption', MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
end; 
  
end.
