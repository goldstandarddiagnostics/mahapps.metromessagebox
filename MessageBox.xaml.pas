﻿namespace MahApps.Metro;

interface

uses
  System.Collections.Generic,
  System.Windows,
  System.Windows.Controls,
  System.Windows.Data,
  System.Windows.Documents,
  System.Windows.Media,
  System.Windows.Navigation,
  System.Windows.Shapes, 
  MahApps.Metro.Controls;

type
  MessageBox = public partial class(MetroWindow)
  private
    fSound: System.Media.SystemSound := nil;
    fdefaultResult: MessageBoxResult; 
    method btnCancel_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    method btnOK_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    method btnNo_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    method btnYes_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    constructor; 
    method SetImageSource(image: MessageBoxImage);
    class method ShowCore(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton; icon: MessageBoxImage; defaultResult: MessageBoxResult): MessageBoxResult;
    method InitializeMessageBox(owner: Window; text: System.String; caption: System.String; button: MessageBoxButton; image: MessageBoxImage; defaultResult: MessageBoxResult);
    method Window_ContentRendered(sender: System.Object; e: System.EventArgs);
  public   
    property MessageBoxResult: MessageBoxResult;   
    class method Show(owner: Window; messageText: System.String): MessageBoxResult;
    class method Show(owner: Window; messageText: System.String; caption: System.String): MessageBoxResult;
    class method Show(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton): MessageBoxResult;
    class method Show(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton; icon: MessageBoxImage): MessageBoxResult;
    class method Show(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton; icon: MessageBoxImage; defaultResult: MessageBoxResult): MessageBoxResult;
  end;
  
implementation

uses 
  System.Windows.Media.Imaging;

constructor MessageBox;
begin
  InitializeComponent();
end;

method MessageBox.btnCancel_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  self.DialogResult := false;
  self.MessageBoxResult := MessageBoxResult.Cancel;
  self.Close();
end;

method MessageBox.btnOK_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  self.DialogResult := true;
  self.MessageBoxResult := MessageBoxResult.OK;
  self.Close();
end;

method MessageBox.btnNo_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  self.DialogResult := false;
  self.MessageBoxResult := MessageBoxResult.No;
  self.Close();
end;

method MessageBox.btnYes_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  self.DialogResult := true;
  self.MessageBoxResult := MessageBoxResult.Yes;
  self.Close();
end;

class method MessageBox.Show(owner: Window; messageText: System.String; caption: System.String): MessageBoxResult;
begin
  exit Show(owner, messageText, caption, MessageBoxButton.OK);
end;

class method MessageBox.Show(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton): MessageBoxResult;
begin
  exit ShowCore(owner, messageText, caption, button, MessageBoxImage.None, MessageBoxResult.None);
end;

class method MessageBox.Show(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton; icon: MessageBoxImage): MessageBoxResult;
begin
  exit ShowCore(owner, messageText, caption, button, icon, MessageBoxResult.None);
end;

class method MessageBox.Show(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton; icon: MessageBoxImage; defaultResult: MessageBoxResult): MessageBoxResult;
begin
  exit ShowCore(owner, messageText, caption, button, icon, defaultResult);
end;

method MessageBox.SetImageSource(image: MessageBoxImage);
begin
  case image of 
    MessageBoxImage.Error:
    begin
      self.pthIcon.Data := self.Resources["appbar_stop"] as Geometry;
      self.pthIcon.Fill := new SolidColorBrush(Color.FromRgb(168,10,10));
      self.pthIcon.Visibility := Visibility.Visible;  
      self.fSound := System.Media.SystemSounds.Hand;
    end;
    MessageBoxImage.Information: 
    begin
      self.pthIcon.Data := self.Resources["appbar_information_circle"] as Geometry;
      self.pthIcon.Fill := new SolidColorBrush(Color.FromRgb(65,177,255));
      self.pthIcon.Visibility := Visibility.Visible;  
      self.fSound := System.Media.SystemSounds.Asterisk;
    end;
    MessageBoxImage.Question:   
    begin
      self.pthIcon.Data := self.Resources["appbar_question"] as Geometry;
      self.pthIcon.Fill := new SolidColorBrush(Color.FromRgb(65,177,255));
      self.pthIcon.Visibility := Visibility.Visible;  
      self.fSound := System.Media.SystemSounds.Question;
    end;
    MessageBoxImage.Warning:
    begin
      self.pthIcon.Data := self.Resources["appbar_warning"] as Geometry;
      self.pthIcon.Fill := new SolidColorBrush(Color.FromRgb(244,185,0));
      self.pthIcon.Visibility := Visibility.Visible;  
      self.fSound := System.Media.SystemSounds.Exclamation;
    end;     
  else
    self.pthIcon.Visibility := Visibility.Collapsed;
    self.fSound := nil;
    exit;    
  end;
end;

class method MessageBox.ShowCore(owner: Window; messageText: System.String; caption: System.String; button: MessageBoxButton; icon: MessageBoxImage; defaultResult: MessageBoxResult): MessageBoxResult;
begin
  var msgBox := new MessageBox();
  msgBox.InitializeMessageBox(owner, messageText, caption, button, icon, defaultResult);
  msgBox.ShowDialog();
  exit msgBox.MessageBoxResult
end;

method MessageBox.InitializeMessageBox(owner: Window; text: System.String; caption: System.String; button: MessageBoxButton; image: MessageBoxImage; defaultResult: MessageBoxResult);
begin
  lblMessage.Text := text;
  if image = MessageBoxImage.None then lblMessage.Width:=720 else lblMessage.Width := 630;
  self.Title := caption;
  case button of
    MessageBoxButton.OK:
    begin
      btnOK.Visibility := Visibility.Visible;
      btnCancel.Visibility := Visibility.Collapsed;
      btnYes.Visibility := Visibility.Collapsed;
      btnNo.Visibility := Visibility.Collapsed;
    end;
    MessageBoxButton.OKCancel:
    begin
      btnOK.Visibility := Visibility.Visible;
      btnCancel.Visibility := Visibility.Visible;
      btnYes.Visibility := Visibility.Collapsed;
      btnNo.Visibility := Visibility.Collapsed;
    end;
    MessageBoxButton.YesNo:
    begin
      btnOK.Visibility := Visibility.Collapsed;
      btnCancel.Visibility := Visibility.Collapsed;
      btnYes.Visibility := Visibility.Visible;
      btnNo.Visibility := Visibility.Visible;
    end;
    MessageBoxButton.YesNoCancel:
    begin
      btnOK.Visibility := Visibility.Collapsed;
      btnCancel.Visibility := Visibility.Visible;
      btnYes.Visibility := Visibility.Visible;
      btnNo.Visibility := Visibility.Visible;
    end;
  end;
  fdefaultResult := defaultResult;
  self.Owner := owner;
  SetImageSource(image)
end;

method MessageBox.Window_ContentRendered(sender: System.Object; e: System.EventArgs);
begin
  if assigned(fSound) then fSound.Play();
end;

class method MessageBox.Show(owner: Window; messageText: System.String): MessageBoxResult;
begin
  exit ShowCore(owner, messageText, '', MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult.None);
end;
 
end.
